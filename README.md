Proyecto api rest básico
Este proyecto está diseñado para cumplir con la funcionalidad de los servicios que necesita un portal backend.

Instalación
Descarga código:
git clone git@gitlab.com:public-projects-basic/back-api-rest.git
Nos dirigimos dentro del proyecto:
cd back-api-rest
Dentro ejecutamos el comando Maven:
mvn clean install
Para ejecutar la aplicación nos dirigimos a:
cd api/target/
Y para ejecutar la aplicación:
java -jar java -jar api-0.0.1-SNAPSHOT.jar
Y accedemos a la aplicación:
http://localhost:8080/api/v2/swagger-ui.html
