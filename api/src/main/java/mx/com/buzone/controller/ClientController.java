package mx.com.buzone.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import mx.com.buzone.model.Cliente;
import mx.com.buzone.service.ClienteService;

@RestController
@RequestMapping("/clients")
public class ClientController extends BaseController{

	private ClienteService clienteService;

	public ClientController(ClienteService clienteService) {
		this.clienteService = clienteService;
	}

	@RequestMapping(method = RequestMethod.GET)
	public List<Cliente> getClients() {
		return clienteService.getAllClients();
	}

	@RequestMapping(value = "/{clientId}", method = RequestMethod.GET)
	public Cliente getClientById(@PathVariable int clientId) {
		return clienteService.getClientById(clientId);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> save(@RequestBody Cliente cliente) {
		try {
			System.out.println(cliente);
			clienteService.insert(cliente);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/{clientId}", method = RequestMethod.PUT)
	public ResponseEntity<Void> update(@PathVariable int clientId, @RequestBody Cliente cliente) {
		try {
			cliente.setId(clientId);

			clienteService.update(cliente);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/{clientId}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable int clientId) {
		try {

			clienteService.delete(clientId);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
}
