package mx.com.buzone.dao.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class DatabaseConfig {

	@Bean
	@Primary
	@ConfigurationProperties("spring.datasource.accounting")
	public DataSourceProperties DataSourceProperties() {
		return new DataSourceProperties();
	}

	@Bean
	@Primary
	@ConfigurationProperties("spring.datasource.accounting")
	@Qualifier("contabilityDataSource")
	public DataSource DataSource() {
		return DataSourceProperties().initializeDataSourceBuilder().build();
	}

}
