package mx.com.buzone.dao.impl;

import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import mx.com.buzone.dao.ClientDao;
import mx.com.buzone.dao.mapper.ClientMapper;
import mx.com.buzone.model.Cliente;

@Repository
public class ClientDaoImpl implements ClientDao {

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public ClientDaoImpl(@Qualifier("contabilityDataSource") DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public List<Cliente> getAllClients() {
		String sql = "select * from test.cliente where deleted_at is null";
		List<Cliente> list = this.jdbcTemplate.query(sql, new ClientMapper());
		return list;
	}

	public Cliente getClientById(int id) {
		String sql = "select * from test.cliente where deleted_at is null and id = ?";
		Cliente cliente = this.jdbcTemplate.queryForObject(sql, new Object[] { id }, new ClientMapper());
		return cliente;
	}

	public void insert(Cliente client) {
		String sql = "insert into test.cliente (nombre, apellido_paterno, apellido_materno, fecha_nacimiento, created_at) values(?, ?, ?, ?, ?)";
		jdbcTemplate.update(sql, client.getNombre(), client.getApellidoPaterno(), client.getApellidoMaterno(),
				client.getFechaNacimiento(), new Date());
	}

	public void update(Cliente client) {
		String sql = "update test.cliente set nombre = ?, apellido_paterno = ?, apellido_materno = ?, fecha_nacimiento = ?, updated_at = ? where id = ?";
		jdbcTemplate.update(sql, client.getNombre(), client.getApellidoPaterno(), client.getApellidoMaterno(),
				client.getFechaNacimiento(), new Date(), client.getId());
	}

	public void delete(int id) {
		String sql = "update test.cliente set deleted_at = ? where id = ?";
		jdbcTemplate.update(sql, new Date(), id);
	}
}
