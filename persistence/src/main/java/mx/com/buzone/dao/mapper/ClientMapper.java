package mx.com.buzone.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.com.buzone.model.Cliente;

public class ClientMapper implements RowMapper<Cliente> {

	public Cliente mapRow(ResultSet resultSet, int rowNum) throws SQLException {
		Cliente cliente = new Cliente();
		cliente.setId(resultSet.getInt("id"));
		cliente.setNombre(resultSet.getString("nombre"));
		cliente.setApellidoPaterno(resultSet.getString("apellido_paterno"));
		cliente.setApellidoMaterno(resultSet.getString("apellido_materno"));
		cliente.setFechaNacimiento(resultSet.getDate("fecha_nacimiento").toLocalDate());
		cliente.setCreatedAt(resultSet.getTimestamp("created_at").toLocalDateTime());
		cliente.setUpdatedAt(
				resultSet.getTimestamp("updated_at") != null ? resultSet.getTimestamp("updated_at").toLocalDateTime()
						: null);
		cliente.setDeletedAt(
				resultSet.getTimestamp("deleted_at") != null ? resultSet.getTimestamp("deleted_at").toLocalDateTime()
						: null);
		return cliente;
	}

}
