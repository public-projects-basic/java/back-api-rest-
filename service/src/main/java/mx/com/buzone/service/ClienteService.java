package mx.com.buzone.service;

import java.util.List;

import mx.com.buzone.model.Cliente;

public interface ClienteService {
	List<Cliente> getAllClients();

	Cliente getClientById(int id);

	void insert(Cliente client);

	void update(Cliente client);

	void delete(int clientId);
}
