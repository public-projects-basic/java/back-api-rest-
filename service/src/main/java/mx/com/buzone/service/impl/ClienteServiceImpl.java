package mx.com.buzone.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.buzone.dao.ClientDao;
import mx.com.buzone.model.Cliente;
import mx.com.buzone.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService {

	private ClientDao clientDao;

	@Autowired
	public ClienteServiceImpl(ClientDao clientDao) {
		this.clientDao = clientDao;
	}

	@Override
	public List<Cliente> getAllClients() {
		return clientDao.getAllClients();
	}

	@Override
	public Cliente getClientById(int id) {
		return clientDao.getClientById(id);
	}

	@Override
	public void insert(Cliente client) {
		clientDao.insert(client);
	}

	@Override
	public void update(Cliente client) {
		clientDao.update(client);
	}

	@Override
	public void delete(int clientId) {
		clientDao.delete(clientId);
	}

}
